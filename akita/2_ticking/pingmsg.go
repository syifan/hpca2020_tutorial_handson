package main

import (
	"gitlab.com/akita/akita"
)

type PingMsg struct {
	akita.MsgMeta

	SeqID int
}

func (p *PingMsg) Meta() *akita.MsgMeta {
	return &p.MsgMeta
}
