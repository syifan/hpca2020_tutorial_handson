package main

import (
	"gitlab.com/akita/akita"
)

func main() {
	engine := akita.NewSerialEngine()
	agentA := NewPingAgent("AgentA", engine)
	agentB := NewPingAgent("AgentB", engine)
	conn := akita.NewDirectConnection("Conn", engine, 1*akita.GHz)

	// Documentation for connection
	// https://pkg.go.dev/gitlab.com/akita/akita?tab=doc#Connection
	conn.______(agentA.OutPort, 1)
	conn.______(agentB.OutPort, 1)

	e1 := StartPingEvent{
		EventBase: akita.NewEventBase(1, agentA),
		Dst:       agentB.OutPort,
	}
	e2 := StartPingEvent{
		EventBase: akita.NewEventBase(3, agentA),
		Dst:       agentB.OutPort,
	}

	// Documentation for engine
	// https://pkg.go.dev/gitlab.com/akita/akita?tab=doc#Engine
	engine._________(e1)
	engine._________(e2)

	// Run the simulation
	engine._________()
	// Output:
	// Ping 0, 2.00
	// Ping 1, 2.00
}
