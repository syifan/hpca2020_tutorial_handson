package main

import (
	"gitlab.com/akita/akita"
)

type StartPingEvent struct {
	*akita.EventBase
	Dst akita.Port
}
